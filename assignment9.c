#include <stdio.h>
#include <stdlib.h>
#define MAX_CHAR_SIZE 100

int main()
{
    FILE *fileW, *fileR, *fileA;
    char content[MAX_CHAR_SIZE];

    fileW = fopen("assignment9.txt", "w");
    fprintf(fileW, "UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fileW);

    fileR = fopen("assignment9.txt", "r");
    while(!feof(fileR))
    {
        fgets(content, MAX_CHAR_SIZE, fileR);
        puts(content);
    }
    fclose(fileR);

    fileA = fopen("assignment9.txt", "a");
    fprintf(fileA, "\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fileA);

    return 0;
}
